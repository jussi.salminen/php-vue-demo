Requirements
- Yarn + NodeJS
- PHP 7.x
- Composer
- mysql/mariadb

To install
 ```
 composer install
 yarn install
 bin/console doctrine:schema:update --force
 ```
 Running test
 ```
  vendor/bin/simple-phpunit
 ```