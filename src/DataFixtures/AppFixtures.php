<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Customer;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class AppFixtures
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->addCategories($manager);
        $this->addCustomers($manager);
        $this->addProducts($manager);

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    private function addCustomers(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $product = new Customer();
            $product->setFirstname(sprintf("Firstname %s", $i + 1));
            $product->setLastname(sprintf("Lastname %s", $i + 1));
            $manager->persist($product);
        }
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    private function addCategories(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++) {
            $product = new Category();
            $product->setName(sprintf("Demo category %s", $i + 1));
            $manager->persist($product);
        }
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     */
    private function addProducts(ObjectManager $manager)
    {
        $category_id = 1;
        for ($i = 0; $i < 10; $i++) {
            $category = $manager->getRepository(Category::class)->find($category_id);
            $product = new Product();
            $product->setName(sprintf("Demo product %s", $i + 1));
            $product->setCategory($category);
            $manager->persist($product);

            if ($i % 2 == 1) {
                $category_id++;
            }
        }
    }
}
