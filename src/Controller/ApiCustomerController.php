<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Traits\HydrateResultTrait;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ApiCustomerController
 *
 * @Route("/api/customer")
 * @package App\Controller
 */
class ApiCustomerController extends Controller
{
    use HydrateResultTrait;

    /**
     * @Route("/create", name="api.customer.create")
     * @Method({"POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $entity = new Customer();
        $entity->setFirstname($request->get('firstname'));
        $entity->setLastname($request->get('lastname'));

        $manager->persist($entity);
        $manager->flush();

        return $this->list();
    }

    /**
     * @Route("/read/{id}", name="api.customer.read")
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function read(int $id)
    {
        $data = $this->getDoctrine()->getRepository(Customer::class)
            ->createQueryBuilder('e')
            ->select('e')
            ->where('e.id = :id')
            ->getQuery()
            ->setParameter('id', $id)
            ->setMaxResults(1)
            ->getResult(Query::HYDRATE_ARRAY);
        $customer = current($data);
        return $this->json($customer);
    }

    /**
     * Boilerplate for update operation
     */
    public function update()
    {

    }

    /**
     * @Route("/delete", name="api.customer.delete")
     * @Method({"POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function delete(Request $request)
    {
        $id = (int)$request->get('id');

        $manager = $this->getDoctrine()->getManager();
        $customer = $this->getDoctrine()->getRepository(Customer::class)->find($id);

        $manager->remove($customer);
        $manager->flush();

        return $this->list();
    }

    /**
     * @Route("s.json", name="api.customer.list")
     */
    public function list()
    {
        $customers = $this->getArrayResult(Customer::class);

        return $this->json($customers);
    }
}
