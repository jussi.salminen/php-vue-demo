<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Traits\HydrateResultTrait;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ApiProductsController
 *
 * @Route("/api/product")
 *
 * @package App\Controller
 */
class ApiProductsController extends Controller
{
    use HydrateResultTrait;

    /**
     * @Route("/create", name="api.product.create")
     * @Method({"POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $manager = $this->getDoctrine()->getManager();
        $product = new Product();
        $product->setName($request->get('name'));
        $product->setCategory($this->getDoctrine()->getRepository(Category::class)->find($request->get('category_id')));

        $manager->persist($product);
        $manager->flush();

        return $this->list();
    }

    /**
     * @Route("/delete", name="api.product.delete")
     * @Method({"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        $id = (int)$request->get('id');
        $manager = $this->getDoctrine()->getManager();
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        $manager->remove($product);
        $manager->flush();

        return $this->list();
    }

    /**
     * @Route("/list.json", name="api.products")
     */
    public function list(): JsonResponse
    {
        $data = $this->getDoctrine()->getRepository(Product::class)->findAll();

        $items = [];
        foreach ($data as $node) {
            $items[] = [
                'id' => $node->getId(),
                'name' => $node->getName(),
                'category' => [
                    'id' => $node->getCategory()->getId(),
                    'name' => $node->getCategory()->getName(),
                ]
            ];
        }

        return $this->json($items);
    }
}
