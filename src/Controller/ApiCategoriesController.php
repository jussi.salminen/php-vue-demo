<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Traits\HydrateResultTrait;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ApiCategoriesController
 *
 * @Route("/api/category")
 *
 * @package App\Controller
 */
class ApiCategoriesController extends Controller
{
    use HydrateResultTrait;

    /**
     * @Route("/create", name="api.category.create")
     * @Method({"POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create(Request $request)
    {
        $name = $request->get('name');
        $manager = $this->getDoctrine()->getManager();

        $category = new Category();
        $category->setName($name);

        $manager->persist($category);
        $manager->flush();

        return $this->list();
    }

    /**
     * @Route("/read/{id}", name="api.category.read")
     *
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function read(int $id)
    {
        $data = current($this->getArrayResult(Category::class, $id));
        $products = $this->getDoctrine()->getRepository(Product::class)->findByCategory($id);

        $format = function ($products) {
            $items = [];

            /** @var Product $product */
            foreach ($products as $product) {
                $items[] = [
                    'id' => $product->getId(),
                    'name' => $product->getName(),
                ];
            }

            return $items;
        };;
        $data['products'] = $format($products);

        return $this->json($data);
    }

    /**
     * @Route("/list.json", name="api.category.list")
     */
    public function list()
    {
        $data = $this->getArrayResult(Category::class);

        return $this->json($data);
    }
}
