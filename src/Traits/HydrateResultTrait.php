<?php

namespace App\Traits;

use Doctrine\ORM\Query;

trait HydrateResultTrait
{
    /**
     * @param string $repository
     * @param int|null $id
     * @return mixed
     */
    protected function getArrayResult(string $repository, int $id = null)
    {
        $query = $this->getDoctrine()
            ->getRepository($repository)
            ->createQueryBuilder('e');

        if ($id) {
            $query->where('e.id = :id');
            $query->setParameter('id', $id);
        }

        return
            $query
                ->select('e')
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
    }
}