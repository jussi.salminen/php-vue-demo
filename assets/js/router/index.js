import Vue from 'vue'
import Router from 'vue-router'
import Hello from '../../components/Hello'
import CustomerList from '../../components/CustomerList'
import Products from '../../components/Product'
import Categories from '../../components/Category'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Hello',
            component: Hello
        },
        {
            path: '/customers',
            name: 'Customer',
            component: CustomerList
        },
        {
            path: '/products',
            name: 'Products',
            component: Products
        },
        {
            path: '/categories',
            name: 'Categories',
            component: Categories
        }
    ]
})