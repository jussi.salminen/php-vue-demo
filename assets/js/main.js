import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource'
import router from './router'

Vue.use(VueResource);

Vue.http.options.emulateJSON = true;

const http = Vue.http;

export default http

Vue.config.productionTip = false;

new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: {App}
});
