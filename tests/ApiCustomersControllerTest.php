<?php

namespace App\Tests;

use App\DataFixtures\AppFixtures;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

/**
 * Class ApiCustomersControllerTest
 * @package App\Tests
 */
class ApiCustomersControllerTest extends WebTestCase
{

    /** @var Client */
    private $client;

    public function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            AppFixtures::class
        ]);

        $this->client = $this->makeClient();
    }

    public function testCreate()
    {
        $this->client->request('POST', '/api/customer/create', ['firstname' => 'New', 'lastname' => 'Customer']);

        $json = $this->client->getResponse()->getContent();
        $data = json_decode($json, true);
        $last = end($data);

        $this->assertEquals(['id' => 11, 'firstname' => 'New', 'lastname' => 'Customer'], $last);

        $this->assertStatusCode(200, $this->client);
    }

    public function testRead()
    {
        $this->client->request('GET', '/api/customer/read/1');

        $json = $this->client->getResponse()->getContent();
        $data = json_decode($json, true);

        $this->assertEquals(['id' => 1, 'firstname' => 'Firstname 1', 'lastname' => 'Lastname 1'], $data);

        $this->assertStatusCode(200, $this->client);
    }

    public function testList()
    {
        $this->client->request('GET', '/api/customers.json');

        $json = $this->client->getResponse()->getContent();
        $data = json_decode($json, true);

        $this->assertCount(10, $data);

        $this->assertStatusCode(200, $this->client);
    }

    public function testDelete()
    {
        $this->client->request('POST', '/api/customer/delete', ['id' => 1]);

        $this->client->request('GET', '/api/customers.json');

        $json = $this->client->getResponse()->getContent();
        $data = json_decode($json, true);

        $this->assertCount(9, $data);

        $this->assertStatusCode(200, $this->client);
    }
}
