<?php

namespace App\Tests;

use App\DataFixtures\AppFixtures;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

/**
 * Class ApiCategoriesControllerTest
 * @package App\Tests
 */
class ApiCategoriesControllerTest extends WebTestCase
{
    /** @var Client */
    private $client;

    public function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            AppFixtures::class
        ]);

        $this->client = $this->makeClient();
    }

    public function testCreate()
    {
        $this->client->request('POST', '/api/category/create', ['name' => 'New']);

        $json = $this->client->getResponse()->getContent();
        $data = json_decode($json, true);
        $last = end($data);

        $this->assertEquals(['id' => 6, 'name' => 'New'], $last);

        $this->assertStatusCode(200, $this->client);
    }


    public function testRead()
    {
        $this->client->request('GET', '/api/category/read/1');

        $json = $this->client->getResponse()->getContent();
        $data = json_decode($json, true);

        $this->assertEquals([
            'id' => 1,
            'name' => 'Demo category 1',
            'products' => [
                ['id' => 1, 'name' => 'Demo product 1'],
                ['id' => 2, 'name' => 'Demo product 2'],
            ]
        ], $data);

        $this->assertStatusCode(200, $this->client);
    }

    public function testList()
    {
        $this->client->request('GET', '/api/category/list.json');

        $json = $this->client->getResponse()->getContent();
        $data = json_decode($json, true);

        $this->assertCount(5, $data);

        $this->assertStatusCode(200, $this->client);
    }
}
