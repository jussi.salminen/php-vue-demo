<?php

namespace App\Tests;

use App\DataFixtures\AppFixtures;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

/**
 * Class ApiCategoriesControllerTest
 * @package App\Tests
 */
class ApiProductsControllerTest extends WebTestCase
{
    /** @var Client */
    private $client;

    public function setUp()
    {
        parent::setUp();

        $this->loadFixtures([
            AppFixtures::class
        ]);

        $this->client = $this->makeClient();
    }

    public function testCreate()
    {
        $this->client->request('POST', '/api/product/create', [
            'name' => 'New',
            'category_id' => 1
        ]);

        $json = $this->client->getResponse()->getContent();
        $data = json_decode($json, true);

        $last = end($data);

        $this->assertEquals(['id' => 11, 'name' => 'New', 'category' => ['id' => 1, 'name' => 'Demo category 1']], $last);

        $this->assertStatusCode(200, $this->client);
    }

    public function testDelete()
    {
        $this->client->request('POST', '/api/product/delete', ['id' => 1]);

        $this->client->request('GET', '/api/product/list.json');

        $json = $this->client->getResponse()->getContent();
        $data = json_decode($json, true);

        $this->assertCount(9, $data);

        $this->assertStatusCode(200, $this->client);
    }

    public function testList()
    {
        $this->client->request('GET', '/api/product/list.json');

        $json = $this->client->getResponse()->getContent();
        $data = json_decode($json, true);

        $this->assertCount(10, $data);

        $this->assertStatusCode(200, $this->client);
    }
}
